<?php

try {
    echo 'Current PHP version: ' . phpversion();
    echo '<br />';

    $host = 'db';
    $dbname = 'database';
    $user = 'user';
    $pass = 'pass';
    $dsn = "mysql:host=$host;dbname=$dbname;charset=utf8";
    $conn = new PDO($dsn, $user, $pass);

    echo 'Database connected successfully';
    echo '<br />';
} catch (\Throwable $t) {
    echo 'Error: ' . $t->getMessage();
    echo '<br />';
}


?>
<p>Exercice 1</p>
<br>
<?php
include('hello.php');
?>
<p>Exercice 2</p>
<br>
<?php
include('reverse.php');
?>
<p>Exercice 3</p>
<br>
<?php
include('robot.php');
?>
<p>Exercice 4</p>
<br>
<?php
include('creditCard.php');
?>
<p>Exercice 5</p>
<br>
<?php
include('clock.php');
?>





